# Angular NVD3 Sample

Purpose of this repo is to demonstrate NVD3 charts in an Angular application

## Steps to run
### Install
`npm install`

`cd angular-d3-client && npm install`
### Build
`cd angular-d3-client && npx ng build --prod --base-href '/client/'`

NOTE: This step is not required during development
### Run
Development: `npm run server`
Production: `npm start`
