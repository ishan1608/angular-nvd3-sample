import {AfterContentInit, Component, OnInit} from '@angular/core';
import * as d3 from 'd3';
import * as nv from 'nvd3';
import {DataService} from "../data.service";
import * as _ from 'underscore';

@Component({
    selector: 'app-visits-count',
    templateUrl: './visits-count.component.html',
    styleUrls: ['./visits-count.component.css']
})
export class VisitsCountComponent implements OnInit, AfterContentInit {
    title: string = 'Visits / Day';

    data: any;

    nvd3Data: any;
    chart: any;
    uniqueChecked: boolean = false;

    constructor(private dataService: DataService) {
    }

    ngOnInit() {
    }

    ngAfterContentInit(): void {
        let self = this;
        this.dataService.getData().subscribe((data) => {
            this.data = data;

            let chartConfig = this.getVisitsChartConfig();
            nv.addGraph({
                generate(): any {
                    self.chart = nv.models.discreteBarChart()
                        .width(800)
                        .height(800)
                        .staggerLabels(false)
                        .showValues(true)
                        .duration(250);

                    self.nvd3Data = d3.select('#visits-count svg')
                        .datum(chartConfig.chart);
                    self.nvd3Data.call(self.chart);

                    VisitsCountComponent.slantXAxis();
                    return self.chart;
                }
            });
        });
    }

    static slantXAxis(): void {
        let xTicks = d3.select('.nv-x.nv-axis > g').selectAll('g');
        xTicks.selectAll('text')
            .attr('transform', (d, i, j) => {
                return 'translate (-25, 20) rotate(-45 0,0)';
            });
    }

    getVisitsChartConfig(unique:boolean=false): any {
        let title = unique ? 'Unique Visits / Day' : 'Visits / Day' ;
        return {
            chart: [{
                key: title,
                nonStackable: true,
                values: this.visitsValues(unique)
            }],
            title: title
        };
    }

    visitsValues(unique:boolean = false): any {
        return _.chain(this.data.logs)
            .reject((log) => {
                return !log.user_id;
            })
            .map((log) => {
                log.date_obj = new Date(log.date);
                return log
            })
            .groupBy((log) => {
                let year = log.date_obj.getFullYear();
                let month = log.date_obj.getMonth();
                let date = log.date_obj.getDate();
                return `${year}-${month}-${date}`;
            })
            .mapObject((logs) => {
                if (unique) {
                    return _.chain(logs)
                        .map((log) => {
                            return log.user_id;
                        })
                        .unique()
                        .value()
                        .length;
                }
                return logs.length;
            })
            .map((count, date) => {
                return {
                    x: date,
                    y: count
                };
            })
            .value();
    }

    uniqueCheckChange(checked: boolean): void {
        let chartConfig = this.getVisitsChartConfig(checked);
        this.title = chartConfig.title;
        this.nvd3Data
            .datum(chartConfig.chart)
            .transition()
            .duration(500)
            .call(this.chart);
        VisitsCountComponent.slantXAxis();
    }
}
