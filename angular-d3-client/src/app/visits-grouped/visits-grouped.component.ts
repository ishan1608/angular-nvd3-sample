import {AfterContentInit, Component, OnInit} from '@angular/core';
import * as d3 from 'd3';
import * as nv from 'nvd3';
import {DataService} from "../data.service";
import * as _ from 'underscore';

@Component({
  selector: 'app-visits-grouped',
  templateUrl: './visits-grouped.component.html',
  styleUrls: ['./visits-grouped.component.css']
})
export class VisitsGroupedComponent implements OnInit, AfterContentInit {
    label: string = 'Visits (Grouped) / Day';

    constructor(
        private dataService: DataService
    ) { }

    ngOnInit() {
    }

    ngAfterContentInit(): void {
        this.dataService.getData().subscribe((data) => {
            let activityData = this.chartData(data);

            nv.addGraph({
                generate(): any {
                    let chart = nv.models.multiBarChart()
                        .width(800)
                        .height(800)
                        .duration(250)
                        .stacked(true);

                    d3.select('#data-chart svg')
                        .datum(activityData)
                        .transition()
                        .duration(0)
                        .call(chart);
                    return chart;
                }
            });
        });
    }

    chartData(data: any): any {
        let logsPerDay = _.chain(data.logs)
            .map((log) => {
                log.date_obj = new Date(log.date);
                return log
            })
            .groupBy((log) => {
                let year = log.date_obj.getFullYear();
                let month = log.date_obj.getMonth();
                let date = log.date_obj.getDate();
                return `${year}-${month}-${date}`;
            }).value();


        let visitsPerDay = _.chain(logsPerDay)
            .map((logs, date) => {
                return {
                    x: date,
                    y: logs.length
                };
            })
            .value();

        let uniqueVisitsPerDay = _.chain(logsPerDay)
            .mapObject((logs) => {
                return _.chain(logs)
                    .map((log) => {
                        return log.user_id;
                    })
                    .unique()
                    .value()
                    .length;
            })
            .map((count, date) => {
                return {
                    x: date,
                    y: count
                };
            })
            .value();

        return [{
            key: 'Visits / Day',
            nonStackable: false,
            values: visitsPerDay
        }, {
            key: 'Unique Visits / Day',
            nonStackable: false,
            values: uniqueVisitsPerDay
        }];
    }

}
