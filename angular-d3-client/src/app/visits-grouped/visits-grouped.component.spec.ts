import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitsGroupedComponent } from './visits-grouped.component';

describe('VisitsGroupedComponent', () => {
  let component: VisitsGroupedComponent;
  let fixture: ComponentFixture<VisitsGroupedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitsGroupedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitsGroupedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
