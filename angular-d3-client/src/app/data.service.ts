import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {of} from 'rxjs';
import {tap} from "rxjs/operators";

@Injectable({
    providedIn: 'root'
})
export class DataService {
    public data: {};

    constructor(private httpClient: HttpClient) {
    }

    getData(): any {
        let localData = sessionStorage.getItem('data');
        if (localData) {
            return of(JSON.parse(localData));
        } else {
            return this.httpClient.get('/api/data.json')
                .pipe(
                    tap(data => sessionStorage.setItem('data', JSON.stringify(data))),
                );
        }
    }
}
