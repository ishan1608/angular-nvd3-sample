import {Component} from '@angular/core';
import {OnInit} from "@angular/core";
import {DataService} from "./data.service";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

    constructor(
        private dataService: DataService
    ) {}

    ngOnInit(): void {
        this.dataService.getData().subscribe(data => {
            // Only for development
            localStorage.setItem('data', JSON.stringify(data));
        }, error => {
            alert(error);
        });
    }
}
