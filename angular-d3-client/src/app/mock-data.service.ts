import {Injectable} from '@angular/core';
import * as d3 from 'd3';


@Injectable({
    providedIn: 'root'
})
export class MockDataService {

    constructor() {
    }

    /* Inspired by Lee Byron's test data generator. */
    public stream_layers(n, m, o): any {
        if (arguments.length < 3) o = 0;

        let bump = (a) => {
            let x = 1 / (.1 + Math.random()),
                y = 2 * Math.random() - .5,
                z = 10 / (.1 + Math.random());
            for (let i = 0; i < m; i++) {
                const w = (i / m - y) * z;
                a[i] += x * Math.exp(-w * w);
            }
        };

        return d3.range(n).map(() => {
            let a = [], i;
            for (i = 0; i < m; i++) a[i] = o + o * Math.random();
            for (i = 0; i < 5; i++) bump(a);
            return a.map(MockDataService.stream_index);
        });
    };

    /* Another layer generator using gamma distributions. */
    // noinspection JSUnusedGlobalSymbols
    public stream_waves(n, m): any {
        return d3.range(n).map((i) => {
            return d3.range(m).map((j) => {
                let x = 20 * j / m - i / 3;
                return 2 * x * Math.exp(-.5 * x);
            }).map(MockDataService.stream_index);
        });
    };

    private static stream_index(d, i): any {
        return {x: i, y: Math.max(0, d)};
    }
}
