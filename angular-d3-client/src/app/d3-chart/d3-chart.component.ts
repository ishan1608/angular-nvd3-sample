import {AfterContentInit, Component, OnInit} from '@angular/core';
import * as d3 from 'd3';
import * as nv from 'nvd3';
import {MockDataService} from "../mock-data.service";

@Component({
    selector: 'app-d3-chart',
    templateUrl: './d3-chart.component.html',
    styleUrls: ['./d3-chart.component.css']
})
export class D3ChartComponent implements OnInit, AfterContentInit {

    constructor(private mockDataService: MockDataService) {
    }

    ngOnInit() {
    }

    ngAfterContentInit(): void {
        let test_data = this.mockDataService.stream_layers(3, 128, .1)
            .map((data, i) => {
                return {
                    key: (i === 1) ? 'Non-stackable Stream' + i : 'Stream' + i,
                    nonStackable: (i === 1),
                    values: data
                };
            });
        nv.addGraph({
            generate(): any {
                const width = nv.utils.windowSize().width,
                    height = 800;
                const chart = nv.models.multiBarChart()
                    .width(width)
                    .height(height)
                    .stacked(true)
                ;
                chart.dispatch.on('renderEnd', () => {
                    console.log('Render Complete');
                });
                const svg = d3.select('#test1 svg').datum(test_data);
                console.log('calling chart');
                svg.transition().duration(0).call(chart);
                return chart;
            },
            callback(graph): void {
                nv.utils.windowResize(() => {
                    const width = nv.utils.windowSize().width;
                    const height = 800;
                    graph.width(width).height(height);
                    d3.select('#test1 svg')
                        .attr('width', width)
                        .attr('height', height)
                        .transition().duration(0)
                        .call(graph);
                });
            }
        });
    }
}
