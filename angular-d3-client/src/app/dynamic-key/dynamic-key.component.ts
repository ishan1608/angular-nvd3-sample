import {AfterContentInit, Component, OnInit} from '@angular/core';
import * as d3 from 'd3';
import * as nv from 'nvd3';
import {DataService} from "../data.service";
import * as _ from 'underscore';

@Component({
    selector: 'app-dynamic-key',
    templateUrl: './dynamic-key.component.html',
    styleUrls: ['./dynamic-key.component.css']
})
export class DynamicKeyComponent implements OnInit, AfterContentInit {
    data: any;
    keys: any;

    nvd3Data: any;
    chart: any;

    uniqueChecked: boolean = false;
    selectedYKey: string = 'user_id';
    selectedXKey: string = 'date_only';

    title: string = DynamicKeyComponent.getChartTitle(this.selectedYKey, this.selectedXKey);

    constructor(
        private dataService: DataService,
    ) {
    }

    ngOnInit() {
    }

    ngAfterContentInit(): void {
        let self = this;
        this.dataService.getData()
            .subscribe((data) => {
                this.data = data;

                this.keys = _.keys(_.first(data.logs));
                this.keys.push('date_only');

                let chartConfig = this.getSelectedKeyBasedChartConfig();
                nv.addGraph({
                    generate(): any {
                        self.chart = nv.models.discreteBarChart()
                            .width(800)
                            .height(800)
                            .staggerLabels(false)
                            .showValues(true)
                            .duration(250);

                        self.nvd3Data = d3.select('#nvd3-chart svg')
                            .datum(chartConfig.chart);
                        self.nvd3Data.call(self.chart);

                        DynamicKeyComponent.slantXAxis();
                        return self.chart;
                    }
                });
            });
    }

    static slantXAxis(): void {
        let xTicks = d3.select('.nv-x.nv-axis > g').selectAll('g');
        xTicks.selectAll('text')
            .attr('transform', (d, i, j) => {
                return 'translate (-25, 20) rotate(-45 0,0)';
            });
    }

    static getChartTitle(yKey:string='', xKey:string='', unique:boolean=false) {
        return unique ? `Unique ${yKey} / ${xKey}` : `${yKey} / ${xKey}`;
    }

    getSelectedKeyBasedChartConfig(unique:boolean=false): any {
        let title = DynamicKeyComponent.getChartTitle(this.selectedYKey, this.selectedXKey, unique);
        return {
            chart: [{
                key: title,
                nonStackable: true,
                values: this.selectedKeyValues(unique)
            }],
            title: title
        };
    }

    selectedKeyValues(unique:boolean = false): any {
        return _.chain(this.data.logs)
            .reject((log) => {
                return !log[this.selectedYKey];
            })
            .map((log) => {
                let dateObj = new Date(log.date);
                let year = dateObj.getFullYear();
                let month = dateObj.getMonth();
                let date = dateObj.getDate();
                log.date_only = `${year}-${month}-${date}`;
                return log
            })
            .groupBy((log) => {
                return log[this.selectedXKey];
            })
            .mapObject((logs) => {
                if (unique) {
                    return _.chain(logs)
                        .map((log) => {
                            return log[this.selectedYKey];
                        })
                        .unique()
                        .value()
                        .length;
                }
                return logs.length;
            })
            .map((count, value) => {
                return {
                    x: value,
                    y: count
                };
            })
            .value();
    }

    selectionChanged(): void {
        let chartConfig = this.getSelectedKeyBasedChartConfig(this.uniqueChecked);
        this.title = chartConfig.title;
        this.nvd3Data
            .datum(chartConfig.chart)
            .transition()
            .duration(500)
            .call(this.chart);
        DynamicKeyComponent.slantXAxis();
    }

}
