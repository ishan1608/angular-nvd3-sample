import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicKeyComponent } from './dynamic-key.component';

describe('DynamicKeyComponent', () => {
  let component: DynamicKeyComponent;
  let fixture: ComponentFixture<DynamicKeyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicKeyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicKeyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
