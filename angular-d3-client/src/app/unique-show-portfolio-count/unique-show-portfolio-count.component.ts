import { Component, OnInit } from '@angular/core';
import {ShowPortfolioCountComponent} from "../show-portoflio-count/show-portoflio-count.component";
import * as _ from 'underscore';


@Component({
  selector: 'app-unique-show-portfolio-count',
  templateUrl: './unique-show-portfolio-count.component.html',
  styleUrls: ['./unique-show-portfolio-count.component.css']
})
export class UniqueShowPortfolioCountComponent extends ShowPortfolioCountComponent implements OnInit {
    label: string = 'Unique Show Portfolio / Day';

    chartData(data: any): any {
        return _.chain(data.logs)
            .map((log) => {
                log.date_obj = new Date(log.date);
                return log
            })
            .groupBy((log) => {
                let year = log.date_obj.getFullYear();
                let month = log.date_obj.getMonth();
                let date = log.date_obj.getDate();
                return `${year}-${month}-${date}`;
            })
            .mapObject((logs) => {
                return _.chain(logs)
                    .filter((log) => {
                        return log.action === 'SHOW_PORTFOLIO';
                    })
                    .map((log) => {
                        return log.user_id;
                    })
                    .unique()
                    .value()
                    .length;
            })
            .map((count, date) => {
                return {
                    x: date,
                    y: count
                };
            })
            .value();
    }
}
