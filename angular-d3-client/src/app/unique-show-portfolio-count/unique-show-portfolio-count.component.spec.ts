import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UniqueShowPortfolioCountComponent } from './unique-show-portfolio-count.component';

describe('UniqueShowPortfolioCountComponent', () => {
  let component: UniqueShowPortfolioCountComponent;
  let fixture: ComponentFixture<UniqueShowPortfolioCountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UniqueShowPortfolioCountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UniqueShowPortfolioCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
