import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {VisitsCountComponent} from "./visits-count/visits-count.component";
import {ShowPortfolioCountComponent} from "./show-portoflio-count/show-portoflio-count.component";
import {UniqueShowPortfolioCountComponent} from "./unique-show-portfolio-count/unique-show-portfolio-count.component";
import {VisitsGroupedComponent} from "./visits-grouped/visits-grouped.component";
import {AgGridSampleComponent} from "./data-ag-grid/data-ag-grid.component";
import {DynamicKeyComponent} from "./dynamic-key/dynamic-key.component";
import {DynamicActionComponent} from "./dynamic-action/dynamic-action.component";


const routes: Routes = [
    {path: '', component: AgGridSampleComponent},
    {path: 'visits-per-day', component: VisitsCountComponent},
    {path: 'show-portfolio-per-day', component: ShowPortfolioCountComponent},
    {path: 'unique-show-portfolio-per-day', component: UniqueShowPortfolioCountComponent},
    {path: 'visits-grouped-per-day', component: VisitsGroupedComponent},
    {path: 'dynamic-keys', component: DynamicKeyComponent},
    {path: 'dynamic-actions', component: DynamicActionComponent},
    {path: '**', redirectTo: ''},
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
