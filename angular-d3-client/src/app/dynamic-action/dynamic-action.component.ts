import {AfterContentInit, Component, OnInit} from '@angular/core';
import * as d3 from 'd3';
import * as nv from 'nvd3';
import * as _ from 'underscore';
import {DataService} from "../data.service";

@Component({
    selector: 'app-dynamic-action',
    templateUrl: './dynamic-action.component.html',
    styleUrls: ['./dynamic-action.component.css']
})
export class DynamicActionComponent implements OnInit, AfterContentInit {
    data: any;
    keys: any;
    actions: any;

    nvd3Data: any;
    chart: any;

    selectedYKey: string = 'LOAD';
    selectedXKey: string = 'date_only';

    title: string = DynamicActionComponent.getChartTitle(this.selectedYKey, this.selectedXKey);

    constructor(
        private dataService: DataService,
    ) {
    }

    ngOnInit() {
    }

    ngAfterContentInit(): void {
        let self = this;
        this.dataService.getData()
            .subscribe((data) => {
                this.data = data;

                this.keys = _.keys(_.first(data.logs));
                this.keys.push('date_only');
                this.actions = this.getActions();

                let chartConfig = this.getSelectedKeyBasedChartConfig();
                nv.addGraph({
                    generate(): any {
                        self.chart = nv.models.discreteBarChart()
                            .width(800)
                            .height(800)
                            .staggerLabels(false)
                            .showValues(true)
                            .duration(250);

                        self.nvd3Data = d3.select('#nvd3-chart svg')
                            .datum(chartConfig.chart);
                        self.nvd3Data.call(self.chart);

                        DynamicActionComponent.slantXAxis();
                        return self.chart;
                    }
                });
            });
    }

    getActions(): any {
        return _.chain(this.data.logs)
            .reject((log) => {
                return !log.action;
            })
            .map((log) => {
                return log.action;
            })
            .unique()
            .value();
    }

    static slantXAxis(): void {
        let xTicks = d3.select('.nv-x.nv-axis > g').selectAll('g');
        xTicks.selectAll('text')
            .attr('transform', (d, i, j) => {
                return 'translate (-25, 20) rotate(-45 0,0)';
            });
    }

    static getChartTitle(yKey:string='', xKey:string='') {
        return `${yKey} / ${xKey}`;
    }

    getSelectedKeyBasedChartConfig(): any {
        let title = DynamicActionComponent.getChartTitle(this.selectedYKey, this.selectedXKey);
        return {
            chart: [{
                key: title,
                nonStackable: true,
                values: this.selectedKeyValues()
            }],
            title: title
        };
    }

    selectedKeyValues(): any {
        return _.chain(this.data.logs)
            .reject((log) => {
                return !log.action;
            })
            .map((log) => {
                let dateObj = new Date(log.date);
                let year = dateObj.getFullYear();
                let month = dateObj.getMonth();
                let date = dateObj.getDate();
                log.date_only = `${year}-${month}-${date}`;
                return log
            })
            .groupBy((log) => {
                return log[this.selectedXKey];
            })
            .mapObject((logs) => {
                return _.filter(logs, (log) => {
                    return log.action === this.selectedYKey;
                }).length;
            })
            .map((count, value) => {
                return {
                    x: value,
                    y: count
                };
            })
            .value();
    }

    selectionChanged(): void {
        let chartConfig = this.getSelectedKeyBasedChartConfig();
        this.title = chartConfig.title;
        this.nvd3Data
            .datum(chartConfig.chart)
            .transition()
            .duration(500)
            .call(this.chart);
        DynamicActionComponent.slantXAxis();
    }
}
