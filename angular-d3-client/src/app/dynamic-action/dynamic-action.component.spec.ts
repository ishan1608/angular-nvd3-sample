import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicActionComponent } from './dynamic-action.component';

describe('DynamicActionComponent', () => {
  let component: DynamicActionComponent;
  let fixture: ComponentFixture<DynamicActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
