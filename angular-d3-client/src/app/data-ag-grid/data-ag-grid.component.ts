import {Component, OnInit, ViewChild} from '@angular/core';
import {AgGridNg2} from "ag-grid-angular";
import {DataService} from "../data.service";
import * as _ from 'underscore';

@Component({
    selector: 'app-data-ag-grid',
    templateUrl: './data-ag-grid.component.html',
    styleUrls: ['./data-ag-grid.component.css']
})
export class AgGridSampleComponent implements OnInit {
    @ViewChild('agGrid') agGrid: AgGridNg2;
    title = 'app';

    columnDefs: {} = [
        {headerName: 'Date', field: 'date_only', rowGroupIndex: 0},
        {headerName: 'Payload', field: 'payload'},
        {headerName: 'User Id', field: 'user_id'},
    ];

    autoGroupColumnDef: any = {
        headerName: 'Action',
        field: 'action',
        cellRenderer: 'agGroupCellRenderer',
        cellRendererParams: {
            checkbox: true
        }
    };

    rowData: {};

    groupByMode: string = 'date';
    groupByText = 'Group By Action';

    constructor(
        private dataService: DataService
    ) {
    }

    ngOnInit() {
        this.dataService.getData().subscribe((data) => {
            this.rowData = _.chain(data.logs)
                .map((log) => {
                    let dateObj = new Date(log.date);
                    let year = dateObj.getFullYear();
                    let month = dateObj.getMonth();
                    let date = dateObj.getDate();
                    log.date_only = `${year}-${month}-${date}`;
                    return log
                })
                .value();
        });
        // Setting the data after subscribing also works. Just don't use 'async' pipe with the ag-grid component
        // this.httpClient.get('https://api.myjson.com/bins/15psn9').subscribe((data) => this.rowData = data);
    }

    groupByAction() {
        if (this.groupByMode === 'date') {
            this.autoGroupColumnDef.headerName = 'Date';
            this.autoGroupColumnDef.field = 'date';

            this.columnDefs = [
                {headerName: 'Action', field: 'action', rowGroupIndex: 0},
                {headerName: 'Payload', field: 'payload'},
                {headerName: 'User Id', field: 'user_id'},
            ];
            this.groupByMode = 'action';
            this.groupByText = 'Group By Date';
        } else {
            this.autoGroupColumnDef.headerName = 'Action';
            this.autoGroupColumnDef.field = 'action';

            this.columnDefs = [
                {headerName: 'Date', field: 'date_only', rowGroupIndex: 0},
                {headerName: 'Payload', field: 'payload'},
                {headerName: 'User Id', field: 'user_id'},
            ];
            this.groupByMode = 'date';
            this.groupByText = 'Group By Action';
        }
    }
}
