import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowPortfolioCountComponent } from './show-portoflio-count.component';

describe('ShowPortfolioCountComponent', () => {
  let component: ShowPortfolioCountComponent;
  let fixture: ComponentFixture<ShowPortfolioCountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowPortfolioCountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowPortfolioCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
