import {AfterContentInit, Component, OnInit} from '@angular/core';
import * as d3 from 'd3';
import * as nv from 'nvd3';
import {DataService} from "../data.service";
import * as _ from 'underscore';

@Component({
    selector: 'app-show-portoflio-count',
    templateUrl: './show-portoflio-count.component.html',
    styleUrls: ['./show-portoflio-count.component.css']
})
export class ShowPortfolioCountComponent implements OnInit, AfterContentInit {
    label: string = 'Show Portfolio / Day';

    constructor(private dataService: DataService) {
    }

    ngOnInit() {
    }

    ngAfterContentInit(): void {
        this.dataService.getData().subscribe((data) => {
            let activityData = this.chartData(data);
            let mapData = [{
                key: 'Show Portfolio clicks / Day',
                nonStackable: true,
                values: activityData
            }];

            nv.addGraph({
                generate(): any {
                    let chart = nv.models.discreteBarChart()
                        .width(800)
                        .height(800)
                        .staggerLabels(false)
                        .showValues(true)
                        .duration(250);

                    d3.select('#data-chart svg')
                        .datum(mapData)
                        .call(chart);

                    let xTicks = d3.select('.nv-x.nv-axis > g').selectAll('g');
                    xTicks
                        .selectAll('text')
                        .attr('transform', (d, i, j) => {
                            return 'translate (-25, 20) rotate(-45 0,0)';
                        });
                    return chart;
                }
            });
        });
    }

    chartData(data: any): any {
        return _.chain(data.logs)
            .map((log) => {
                log.date_obj = new Date(log.date);
                return log
            })
            .groupBy((log) => {
                let year = log.date_obj.getFullYear();
                let month = log.date_obj.getMonth();
                let date = log.date_obj.getDate();
                return `${year}-${month}-${date}`;
            })
            .mapObject((logs) => {
                return _.chain(logs)
                    .filter((log) => {
                        return log.action === 'SHOW_PORTFOLIO';
                    })
                    .value()
                    .length;
            })
            .map((count, date) => {
                return {
                    x: date,
                    y: count
                };
            })
            .value();
    }

}
