import {NgModule} from '@angular/core';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import {BrowserModule} from '@angular/platform-browser'

import {AgGridModule} from "ag-grid-angular";
import 'ag-grid-enterprise';

import {AppComponent} from './app.component';
import {D3ChartComponent} from './d3-chart/d3-chart.component';
import {VisitsCountComponent} from './visits-count/visits-count.component';
import {AppRoutingModule} from './app-routing.module';
import {ShowPortfolioCountComponent} from './show-portoflio-count/show-portoflio-count.component';
import {UniqueShowPortfolioCountComponent} from './unique-show-portfolio-count/unique-show-portfolio-count.component';
import {VisitsGroupedComponent} from './visits-grouped/visits-grouped.component';
import {AgGridSampleComponent} from './data-ag-grid/data-ag-grid.component';
import {DynamicKeyComponent} from './dynamic-key/dynamic-key.component';
import { DynamicActionComponent } from './dynamic-action/dynamic-action.component';

@NgModule({
    declarations: [
        AppComponent,
        D3ChartComponent,
        VisitsCountComponent,
        ShowPortfolioCountComponent,
        UniqueShowPortfolioCountComponent,
        VisitsGroupedComponent,
        AgGridSampleComponent,
        DynamicKeyComponent,
        DynamicActionComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        AgGridModule.withComponents([]),
        FormsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
